public class Solution {
  public static void grenade(int[] list) {
    boolean needNewBreath = true;
    for (int k = 1; k < list.length && needNewBreath; k++){
     needNewBreath = false;
     for (int i = 0; i < list.length - k; i++){
       if (list[i] > list[i + 1]){
         int temp = list[i];
         list[i] = list[i + 1];
         list[i + 1] = temp;
         
         needNewBreath = true;
     }
    }
   }
  }
  
  public static void main(String[] args){
    int[] list = {5, 2, 3, 1,};
    bubbleSort(list);
    for (int i = 0; i < list.length; i++)
      System.out.print(list[i] + " ");
  }
} 
  
    