import java.util.Arrays;
import java.util.Scanner;

public class Car {
	private String color;
	private double engine_capacity;
	private int year;
	private String country;

	public Car() {
		color = "black";
		engine_capacity = 3.5;
		year = 2014;
		country = "Germany";
	}

	public Car(String color, double engine_capacity, int year, String country) {
		this.color = color;
		this.engine_capacity = engine_capacity;
		this.year = year;
		this.country = country; 
	}
		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public double getEngineCapacity() {
			return engine_capacity;
		}

		public void setEngineCapacity() {
			this.engine_capacity = engine_capacity;
		}

		public int getYear() {
			return year;
		}

		public String getCountry() {
			return country;
		}
	
	
class Supercar extends Car 
 implements Comparable<Supercar> {
	private double velocity;
	private double acceleration;
	private int time;

	public Supercar(double velocity, double acceleration, int time) {
		this.velocity = velocity;
		this.acceleration = acceleration;
		this.time = time;
	}
	public double distance() {
		return velocity * time + 0.5 * acceleration * time * time;
	}

	public double finalVelocity() {
		return Math.sqrt(velocity * velocity + 2 * acceleration * distance());
	}

	@Override
	public int compareTo(Supercar car) {
		if(this.distance() > car.distance()) {
			return 1;
		} else if(this.distance() < car.distance()) {
			return -1;
		} else {
			return 0;
		}

		}
	}
}


	class Solution {
		public static void main(String[] args) {
			Scanner in = new Scanner(System.in);
			Car car1  = new Car();
			
			String color = in.next();
			double capacity = in.nextDouble();
			int year = in.nextInt();
			String country = in.next();
			System.out.printf("%s %.1f %d %s\n", car1.getColor(), car1.getEngineCapacity(), car1.getYear(), car1.getCountry());

			Car car2 = new Car(color, capacity, year, country);
			car2.setColor(color);
			
			System.out.printf("%s %.1f %d %s\n", car2.getColor(), car2.getEngineCapacity(), car2.getYear(), car2.getCountry());

			Supercar scar1 = new Supercar(in.nextInt(), in.nextDouble(), in.nextInt());
			Supercar scar2 = new Supercar(in.nextInt(), in.nextDouble(), in.nextInt());
			Supercar scar3 = new Supercar(in.nextInt(), in.nextDouble(), in.nextInt());

			Supercar[] scars = {scar1, scar2, scar3};
			Arrays.sort(scars);

			System.out.printf("%.1f %.1f %.1f\n", scars[0].finalVelocity(), scars[1].finalVelocity(), scars[2].finalVelocity());
			System.out.printf("%.1f %.1f %.1f\n", scars[0].distance(), scars[1].distance(), scars[2].distance());
			scar1.setColor(in.next());
			System.out.printf("%s %s\n", scar1.getCountry(), scar1.getColor());

		}
	}

